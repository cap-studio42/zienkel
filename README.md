# Zienkel

## Brief Product Description

An innovative e-commerce service concept using Cognitive services, AI, Blockchain and second screen applications to create new ways of interaction between viewers and media broadcasters. 

## Product Summary

We build a unique business model to help the collaboration between media broadcasters and viewers via a ‘second screen’ application used with companion devices or in-person real-life events. We enable an enhanced user experience allowing audiences to buy products related to the shows they are watching “now”. Our business strategy leverages its unique ability to design and develop applications with new technology using new concepts in the digital era of the industry, such as Image & Voice recognition, Machine learning, Blockchain. This is a concept significantly different and beyond usual online shopping.
Example 1 – You are watching Big Bang Theory. You see something on Sheldon’s study table or his quirky shirt. You like it and want to buy it. We make it possible. 
Example 2 – You attend a fashion show in person and like the Gucci / Armani suit/accessories you see as the model walks down the ramp. You want to buy it. We make it possible.

## What tools and dataset are you using to develop your solution?

1.	Azure for the core and a webapp for frontend development
	* Azure Cognitive services API  
	* Azure Data Science Virtual Machine 
	* Azure HDInsight & Databricks
	* Azure Blockchain
	* A very attractive frontend for the consumers, a B2B interface for broadcasters.
2.	Datasets 
	*	https://www.kaggle.com/c/imaterialist-challenge-furniture-2018
	*	https://www.kaggle.com/c/imaterialist-challenge-fashion-2018
	*	https://www.kaggle.com/c/nv-brand-logo-recognition2/data
	*	We need some kind of annotated video set, still looking for it now 😊


